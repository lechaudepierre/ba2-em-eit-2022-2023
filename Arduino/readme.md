# Arduino

Ce dossier contient les codes Arduino lié au projet :

- [testMoteur](#testmoteur)
- [testEncodeur](#testencodeur)
- [caracMoteur](#caracmoteur)

Chaque code est accompagné de son schéma de connexion, de son *flowchart* et d'un *readme* expliquant son fonctionnement global.  
De plus, le code est commenté pour faciliter sa lecture.

Les schémas de connexion ont été fait avec [Kicad](https://www.kicad.org/) et les *flowcharts* avec [Yed](https://www.yworks.com/products/yed).

## testMoteur

Ce code permet de tester un moteur (et son *H-bridge*).

Il utilise une *digital output* et une *PWM output*.

## testEncodeur

Ce code permet de tester l'encodeur en quadrature d'un moteur.

Il utilise 2 *digital inputs*.

Il utilise également le mécanisme d'interruption pour détecter les changements des signaux de l'encodeur.

## caracMoteur

Ce code permet de relever la caractéristique statique tension-vitesse d'un moteur.
