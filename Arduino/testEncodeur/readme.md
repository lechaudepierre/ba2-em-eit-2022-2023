# testEncodeur

Code permettant de tester le fonctionnement d'un encodeur en quadrature

L'*Arduino* compte les impulsions d'un [encodeur en quadrature](https://en.wikipedia.org/wiki/Incremental_encoder) et envoie périodiquement le résultat sur son [port série virtuel](https://www.arduino.cc/reference/en/language/functions/communication/serial/).

Le comptage des impulsions est fait par une *routine d'interruption* (*Interrupt Service Routine* ou *ISR* en anglais).  
Le mécanisme des *interruptions* permet à un microprocesseur de réagir à un évènement externe (ici le changement d'état d'un signal venant de l'encodeur) sans avoir à constamment vérifier, dans le programme principal (*loop()*) si cet évènement a eu lieu.  
Lorque l'évènement se produit :

* l'exécution du programme pricnipal est interrompue
* l'*ISR* est exécutée
* l'exécution du programme principal reprend.

2 remarques importantes :

* l'*ISR* est une fonction paramètre et ne renvoyant aucune valeur.  Elle est exécutée automatiquement en réponse à l'évènement auquel elle est associée.  **Elle ne doit pas être appelée par le programme principal**
* L'interruption est transparente pour le programme principal.  Il ne sait pas qu'il a été interrompu.

Plus d'infos sur l'utilisation des interruptions [ici](http://gammon.com.au/interrupts).

## Schéma du montage

![Montage de testEncodeur](testEncSch.png)

L'[*Arduino Nano Every*](https://docs.arduino.cc/hardware/nano-every) est connecté à l'encodeur d'un moteur DC.  
Celui-ci est alimenté en 5V par l'Arduino.
L'Arduino doit être connecté, par USB, à un PC qui lira les données envoyées (et alimentera le circuit).

## Flowchart

![Flowchart de testEncodeur](testEncFC.png)

Pour cet exemple, il y a en fait 2 flowcharts : celui du programme principal et celui de l'ISR.

# Comportement attendu

L'Arduino envoye la valeur de *tickCount* toutes les 500ms sur son port série.  Ces valeurs peuvent être lue en utilisant le *Serial monitor* de l'*IDE Arduino* (dans le menu *Tools*).

Initialement *tickCount* vaut 0. Si on tourne l'axe du moteur, la valeur doit augmenter ou diminuer, en fonction du sens de rotation.
